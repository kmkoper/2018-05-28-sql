CREATE USER 'homework' IDENTIFIED BY '1234';
CREATE DATABASE homeworkDB CHARACTER SET utf8 COLLATE utf8_general_ci;
GRANT ALL ON homeworkDB.* to 'homework';
exit
mysql -uhomework -p
USE homeworkDB;

CREATE TABLE dragons (
    name VARCHAR(100),
    color VARCHAR(100),
    rangeOfWings NUMERIC(3,0),
    dragonID INT PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE eggs (
    weight NUMERIC(3,0),
    diameter  NUMERIC(3,0),
    eggID INT PRIMARY KEY AUTO_INCREMENT
);

ALTER TABLE eggs ADD COLUMN eggDragonID INT;
ALTER TABLE eggs ADD FOREIGN KEY (eggDragonID) REFERENCES dragons (dragonID);

CREATE TABLE adornment (
    color VARCHAR(100),
    pattern VARCHAR(100),
    adornmentID INT NOT NULL PRIMARY KEY 
);

ALTER TABLE adornment ADD FOREIGN KEY (adornmentID) REFERENCES eggs (eggID);


CREATE TABLE lands (
    landName VARCHAR(100),
    landID INT PRIMARY KEY AUTO_INCREMENT
);

CREATE TABLE dragonsLands (
    dlDragonID INT,
    dlLandID INT,
    dragonsLandsID INT PRIMARY KEY AUTO_INCREMENT
);

ALTER TABLE dragonsLands ADD FOREIGN KEY (dlDragonID) REFERENCES dragons (dragonID);
ALTER TABLE dragonsLands ADD FOREIGN KEY (dlLandID) REFERENCES lands (landID);

INSERT INTO dragons (name, color, rangeOfWings) VALUES ('Dygir', 'green', '200');
INSERT INTO dragons (name, color, rangeOfWings) VALUES ('Bondril', 'red', '100');
INSERT INTO dragons (name, color, rangeOfWings) VALUES ('Onosse', 'black', '250');
INSERT INTO dragons (name, color, rangeOfWings) VALUES ('Chiri', 'yellow', '50');
INSERT INTO dragons (name, color, rangeOfWings) VALUES ('Lase', 'blue', '300');

INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (300, 20, (SELECT dragonID FROM dragons WHERE name = 'Dygir'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('pink', 'mesh', (SELECT eggID FROM eggs WHERE weight = 300 && diameter = 20));

INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (340, 30, (SELECT dragonID FROM dragons WHERE name = 'Dygir'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('black', 'stripped', (SELECT eggID FROM eggs WHERE weight = 340 && diameter = 30));


INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (200, 10, (SELECT dragonID FROM dragons WHERE name = 'Bondril'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('yellow', 'dotted', (SELECT eggID FROM eggs WHERE weight = 200 && diameter = 10));

INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (230, 20, (SELECT dragonID FROM dragons WHERE name = 'Onosse'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('blue', 'dotted', (SELECT eggID FROM eggs WHERE weight = 230 && diameter = 20));

INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (300, 25, (SELECT dragonID FROM dragons WHERE name = 'Onosse'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('green', 'stripped', (SELECT eggID FROM eggs WHERE weight = 300 && diameter = 25));

INSERT INTO eggs (weight, diameter, eggDragonID) VALUES (200, 15, (SELECT dragonID FROM dragons WHERE name = 'Onosse'));
INSERT INTO adornment (color, pattern, adornmentID) VALUES ('red', 'mesh', (SELECT eggID FROM eggs WHERE weight = 200 && diameter = 15));

INSERT INTO lands (landName) VALUES ('froze');
INSERT INTO lands (landName) VALUES ('oswia');
INSERT INTO lands (landName) VALUES ('oscyae');
INSERT INTO lands (landName) VALUES ('oclurg');

INSERT INTO dragonsLands (dlDragonID, dlLandID) VALUES ((SELECT dragonID FROM dragons WHERE name = 'Dygir'), (SELECT landID FROM lands WHERE landName = 'froze'));
INSERT INTO dragonsLands (dlDragonID, dlLandID) VALUES ((SELECT dragonID FROM dragons WHERE name = 'Bondril'), (SELECT landID FROM lands WHERE landName = 'froze'));
INSERT INTO dragonsLands (dlDragonID, dlLandID) VALUES ((SELECT dragonID FROM dragons WHERE name = 'Dygir'), (SELECT landID FROM lands WHERE landName = 'oswia'));
INSERT INTO dragonsLands (dlDragonID, dlLandID) VALUES ((SELECT dragonID FROM dragons WHERE name = 'Onosse'), (SELECT landID FROM lands WHERE landName = 'oswia'));
INSERT INTO dragonsLands (dlDragonID, dlLandID) VALUES ((SELECT dragonID FROM dragons WHERE name = 'Chiri'), (SELECT landID FROM lands WHERE landName = 'oswia'));

CREATE VIEW allAboutEgg AS SELECT weight, diameter, color, pattern FROM eggs, adornment WHERE eggs.eggID = adornment.adornmentID;

CREATE VIEW dragonAndLand AS SELECT name, landName FROM lands, dragons, dragonsLands WHERE lands.landID = dragonsLands.dlLandID AND dragons.dragonID = dragonsLands.dlDragonID;

CREATE VIEW dragonAndEgg AS SELECT name, weight, diameter FROM dragons, eggs WHERE dragons.dragonID = eggs.eggDragonID;

CREATE VIEW dragonsWithoutEggs AS SELECT name FROM dragons LEFT JOIN eggs ON dragons.dragonID = eggs.eggDragonID WHERE eggs.eggDragonID IS NULL;

SELECT * FROM dragons;
SELECT * FROM eggs;
SELECT * FROM adornment;
SELECT * FROM lands;
SELECT * FROM dragonsLands;
SELECT * FROM allAboutEgg;
SELECT * FROM dragonAndLand;
SELECT * FROM dragonAndEgg;
SELECT * FROM dragonsWithoutEggs;

SELECT name FROM dragons WHERE 400 > rangeOfWings && rangeOfWings > 200;

DROP TABLE dragons, eggs, adornment, lands, dragonsLands;